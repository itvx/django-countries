#!/usr/bin/env python
from setuptools import setup, find_packages

setup(name='django-countries',version='6.2.dev0',packages=find_packages())
